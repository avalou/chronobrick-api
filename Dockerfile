# 
# This is an unofficial Alpine image embedding rbenv
# Inspired by https://github.com/didlich/docker-alpine-rbenv/blob/master/Dockerfile
# 

FROM alpine:3.15

RUN apk add --update \
    bash \
    git \
    wget \
    curl \
    vim \
    build-base \
    readline-dev \
    openssl-dev \
    zlib-dev \
&& rm -rf /var/cache/apk/*

# rbenv
ENV PATH /usr/local/rbenv/shims:/usr/local/rbenv/bin:$PATH
ENV RBENV_ROOT /usr/local/rbenv
ENV RUBY_VERSION 3.0.2
ENV CONFIGURE_OPTS --disable-install-doc

RUN apk add --update \
    linux-headers \
    imagemagick-dev \    
    libffi-dev \    
    libffi-dev \
	libpq-dev \
	&& rm -rf /var/cache/apk/*

RUN git clone --depth 1 git://github.com/sstephenson/rbenv.git ${RBENV_ROOT} \
&&  git clone --depth 1 https://github.com/sstephenson/ruby-build.git ${RBENV_ROOT}/plugins/ruby-build \
&&  git clone --depth 1 git://github.com/jf/rbenv-gemset.git ${RBENV_ROOT}/plugins/rbenv-gemset \
&& ${RBENV_ROOT}/plugins/ruby-build/install.sh

RUN echo 'eval "$(rbenv init -)"' >> /etc/profile.d/rbenv.sh 

RUN rbenv install $RUBY_VERSION \
&&  rbenv global $RUBY_VERSION

RUN gem install bundler

WORKDIR /app

# Getting required gems
COPY Gemfile ./
RUN bundle install

COPY [^Gemfile]* ./

# RUN RAILS_ENV=production / db:create
# RUN / db:migrate

# Deleting useless packages
# RUN apk del git wget curl build-base \
#     rm -rf /var/cache/apk/*

EXPOSE 9000

CMD [ "top" ]

# CMD [ "rails", "server", "-p 9000", "-b 127.0.0.1", "-e production" ]