# Chronobriq
## Rails API

Ruby 3.0.1 // Rails 6.1.3.1

Voir [cet article](https://www.digitalocean.com/community/tutorials/how-to-install-ruby-on-rails-with-rbenv-on-ubuntu-18-04) pour installer Ruby avec Rbenv, un gestionnaire de version pour Ruby.  
Installer rails : `gem install rails -v 6.1.3.1`

### 0. Pré-requis

PostgreSQL 11 minimum et la gem associée (`pg`).

### 1. Installation
1. `bundle install`
2. `rails db:migrate` (et `rails db:seed` pour peupler la db avec des données factices, si nécessaire)
3. `rails server` pour lancer un serveur local de développement sur le port 3000

### 2. Endpoints
La commande `rails routes` fournie l'ensemble des endpoints et requêtes HTTP disponibles. L'ensemble des requêtes se font sur la base : domaine.tld/api/v1.

## Tests

Les scripts de test sont situés dans le répertoire `test`, y compris les test distants (`test/remote`).

## Appel de l'API

### Créer un compte utilisatrice
                      
`POST   /api/v1/users(.:format)      api/v1/users#create {:format=>:json}`

```bash
curl --header "Content-Type: application/json" -d "{\"user\" : { \"username\" : \"test\", \"password\" : \"lepwd\", \"email\" : \"iel@cli.su\" }}" -XPOST https://chronobriq-api.URL/api/v1/users 
```

### Obtenir un token

`POST   /api/v1/tokens(.:format)  api/v1/tokens#create {:format=>:json}`

```bash
curl --header "Content-Type: application/json" -d "{\"user\":{\"email\":\"iel@cli.su\", \"password\":\"lepwd\"}}" -XPOST https://chronobriq-api.URL/api/v1/tokens
```

Un obtient une réponse du type :

```
{"token":"Kgt2p1ITAjfbLK4xtS8YPi44gIMWrwtagOGwK0ROEOUgrn6Jj34sqprnX2d7t7RI","email":"jules@cli.coop","username":"test"}
```

#### Usage

Grâce au to.ken obtenu précédemment, nous pouvons récupérer des informations en le fournissant dans l'entête

##### GET

- Users

```
curl --header "Content-Type: application/json" -H "Authorization : Kgt2p1ITAjfbLK4xtS8YPi44gIMWrwtagOGwK0ROEOUgrn6Jj34sqprnX2d7t7RI" -XGET https://chronobriq-api.canaille.netlib.re/api/v1/users
```

- Teams

```
curl --header "Content-Type: application/json" -H "Authorization:  Kgt2p1ITAjfbLK4xtS8YPi44gIMWrwtagOGwK0ROEOUgrn6Jj34sqprnX2d7t7RI" -XGET https://chronobriq-api.canaille.netlib.re/api/v1/teams
```

## Notes

Patch et delete permettent de modifier ou supprimer une donnée qui a été crée depuis cette utilisatrice, après avoir spécifié l'id du jeu de données dans l'URL

### Routes

|                Prefix | Verb   | URI Pattern                                                | Controller#Action                           |
| --------------------- | ------ | ---------------------------------------------------------- | ------------------------------------------- |
|           api_v1_users| GET    | /api/v1/users(.:format)                                    | api/v1/users#index {:format=>:json}         |
|                       | POST   | /api/v1/users(.:format)                                    | api/v1/users#create {:format=>:json}        |
|            api_v1_user| GET    | /api/v1/users/:id(.:format)                                | api/v1/users#show {:format=>:json}          |
|                       | PATCH  | /api/v1/users/:id(.:format)                                | api/v1/users#update {:format=>:json}        |
|                       | PUT    | /api/v1/users/:id(.:format)                                | api/v1/users#update {:format=>:json}        |
|                       | DELETE | /api/v1/users/:id(.:format)                                | api/v1/users#destroy {:format=>:json}       |
|          api_v1_tokens| POST   | /api/v1/tokens(.:format)                                   | api/v1/tokens#create {:format=>:json}       |
|api_v1_team_memberships| GET    | /api/v1/teams/:team_id/memberships(.:format)               | api/v1/memberships#index {:format=>:json}   |
|                       | POST   | /api/v1/teams/:team_id/memberships(.:format)               | api/v1/memberships#create {:format=>:json}  |
| api_v1_team_membership| GET    | /api/v1/teams/:team_id/memberships/:id(.:format)           | api/v1/memberships#show {:format=>:json}    |
|                       |PATCH   | /api/v1/teams/:team_id/memberships/:id(.:format)           | api/v1/memberships#update {:format=>:json}  |
|                       | PUT    | /api/v1/teams/:team_id/memberships/:id(.:format)           | api/v1/memberships#update {:format=>:json}  |
|                       | DELETE | /api/v1/teams/:team_id/memberships/:id(.:format)           | api/v1/memberships#destroy {:format=>:json} |
|          api_v1_teams| GET    | /api/v1/teams(.:format)                                     | api/v1/teams#index {:format=>:json}         |
|                      | POST   | /api/v1/teams(.:format)                                     | api/v1/teams#create {:format=>:json}        |
|           api_v1_team| GET    | /api/v1/teams/:id(.:format)                                 | api/v1/teams#show {:format=>:json}          |
|                      | PATCH  | /api/v1/teams/:id(.:format)                                 | api/v1/teams#update {:format=>:json}        |
|                      | PUT    | /api/v1/teams/:id(.:format)                                 | api/v1/teams#update {:format=>:json}        |
|                      | DELETE | /api/v1/teams/:id(.:format)                                 | api/v1/teams#destroy {:format=>:json}       |
|   api_v1_memberships | GET    | /api/v1/memberships(.:format)                               |api/v1/memberships#index {:format=>:json}    |
| api_v1_activity_tasks| GET    | /api/v1/activities/:activity_id/tasks(.:format)             | api/v1/tasks#index {:format=>:json}         |
|                      | POST   | /api/v1/activities/:activity_id/tasks(.:format)             | api/v1/tasks#create {:format=>:json}        |
|  api_v1_activity_task| GET    | /api/v1/activities/:activity_id/tasks/:id(.:format)         | api/v1/tasks#show {:format=>:json}          |
|                      | PATCH  | /api/v1/activities/:activity_id/tasks/:id(.:format)         | api/v1/tasks#update {:format=>:json}        |
|                      | PUT    | /api/v1/activities/:activity_id/tasks/:id(.:format)         | api/v1/tasks#update {:format=>:json}        |
|                      | DELETE | /api/v1/activities/:activity_id/tasks/:id(.:format)         | api/v1/tasks#destroy {:format=>:json}       |
|     api_v1_activities| GET    | /api/v1/activities(.:format)                                | api/v1/activities#index {:format=>:json}    |
|                      | POST   | /api/v1/activities(.:format)                                | api/v1/activities#create {:format=>:json}   |
|       api_v1_activity| GET    | /api/v1/activities/:id(.:format)                            | api/v1/activities#show {:format=>:json}     |
|                      | PATCH  | /api/v1/activities/:id(.:format)                            | api/v1/activities#update {:format=>:json}   |
|                      | PUT    | /api/v1/activities/:id(.:format)                            | api/v1/activities#update {:format=>:json}   |
|                      | DELETE | /api/v1/activities/:id(.:format)                            | api/v1/activities#destroy {:format=>:json}  |
|          api_v1_tasks| GET    | /api/v1/tasks(.:format)                                     | api/v1/tasks#index {:format=>:json}         |
