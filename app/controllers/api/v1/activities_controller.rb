class Api::V1::ActivitiesController < ApplicationController
  before_action :set_activity, only: %i[show update destroy]
  before_action :check_login

  def index
    render json: ActivitySerializer.new(Activity.all).serializable_hash.to_json
  end

  def show
    render json: ActivitySerializer.new(@activity).serializable_hash.to_json
  end

  def create
    activity = current_user.created_activities.build(activity_params)
    if activity.save
      JoinedUserActivity.create!(
        user_id: current_user.id,
        activity_id: activity.id
      )
      render json: ActivitySerializer.new(activity).serializable_hash.to_json, status: :created
    else
      render json: { errors: activity.errors }, status: :unprocessable_entity
    end
  end

  def update
    if @activity.update(activity_params)
      render json: ActivitySerializer.new(@activity).serializable_hash.to_json, status: :ok
    else
      render json: @activity.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @activity.destroy
    head 204
  end

  private

  # Only allow a trusted parameter "white list" through.
  def activity_params
    params.require(:activity).permit(:name, :author_id, :description, :client)
  end

  def set_activity
    @activity = Activity.find(params[:id])
  end
end
