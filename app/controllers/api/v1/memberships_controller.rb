class Api::V1::MembershipsController < ApplicationController
  before_action :set_memberships, only: %i[show update destroy]
  before_action :check_login

  def index
    if !params[:team_id]
      render json: MembershipSerializer.new(Membership.all).serializable_hash.to_json
    else
      render json: MembershipSerializer.new(Team.find(params[:team_id]).memberships).serializable_hash.to_json
    end
  end
  
  def show
    render json: MembershipSerializer.new(@membership).serializable_hash.to_json
  end

  def create  
    membership = current_user.memberships.build(membership_params)
    membership.team_id = params[:team_id]
  
    if membership.save
      render json: MembershipSerializer.new(membership).serializable_hash.to_json, status: :created
    else
      render json: membership.errors, status: :unprocessable_entity
    end
  end

  def update
    if params[:membership][:can_edit] 
      if @membership.update(membership_params)
        render json: MembershipSerializer.new(@membership).serializable_hash.to_json, status: :ok
      else
        render json: @membership.errors, status: :unprocessable_entity
      end
    else
      head 422
    end
  end
  
  def destroy
    @membership.destroy
    head 204
  end

  private

  def membership_params
    params.require(:membership).permit(:can_edit)
  end
  
  def set_memberships
    @membership = Membership.find(params[:id])
  end
end
