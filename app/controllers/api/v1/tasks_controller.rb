class Api::V1::TasksController < ApplicationController
  before_action :set_task, only: %i[show update destroy]
  before_action :check_login
  before_action :check_owner, only: %i[update destroy]

  def index
    if !params[:activity_id]
      render json: TaskSerializer.new(Task.all).serializable_hash.to_json
    else
      render json: TaskSerializer.new(Activity.find(params[:activity_id]).tasks).serializable_hash.to_json
    end
  end

  def show
    render json: TaskSerializer.new(@task).serializable_hash.to_json
  end

  def create
    task = current_user.tasks.build(task_params)
    task.activity_id = params[:activity_id]
    if task.save
      render json: TaskSerializer.new(task).serializable_hash.to_json,
      status: :created
    else
      render json: { errors: task.errors }, status: :unprocessable_entity
    end
  end

  def update
    if @task.update(task_params)
      render json: TaskSerializer.new(@task).serializable_hash.to_json, status: :ok
    else
      render json: @task.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @task.destroy
    head 204
  end

  private

  def task_params
    params.require(:task).permit(:name, :description, :start_time, :end_time, :is_paid, :paid_by, :owner_id, :activity_id)
  end

  def set_task
    @task = Activity.find(params[:activity_id]).tasks.find(params[:id])
  end

  def check_owner
    head :forbidden unless @task.owner_id === current_user.id
  end
end
