class Api::V1::TeamsController < ApplicationController
  before_action :set_team, only: %i[show update destroy]
  before_action :check_login
  before_action :can_edit?, only: %i[update destroy]

  def index
    render json: TeamSerializer.new(Team.all).serializable_hash.to_json
  end

  def show
    render json: TeamSerializer.new(@team).serializable_hash.to_json
  end

  def create
    team = current_user.teams.build(team_params)

    if team.save 
      if current_user&.is_admin === false
        Membership.create!(
          member_id: current_user.id,
          team_id: team.id,
          can_edit: true
        )
        render json: TeamSerializer.new(team).serializable_hash.to_json, status: :created
      else
        render json: TeamSerializer.new(team).serializable_hash.to_json, status: :created
      end
    else 
      render json: team.errors, status: :unprocessable_entity
    end
  end

  def update
    if @team.update(team_params)
      render json: TeamSerializer.new(@team).serializable_hash.to_json, status: :ok
    else
      render json: @team.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @team.destroy
    head 204
  end

  private
  
  def team_params
    params.require(:team).permit(:name, :description, :created_at, :updated_at)
  end

  def set_team
    @team = Team.find(params[:id])
  end

  def can_edit?
    head :forbidden unless Membership.where(team_id: params[:id]).where(member_id: current_user.id)[0].can_edit
  end
end
