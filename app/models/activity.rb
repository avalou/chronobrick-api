class Activity < ApplicationRecord
  belongs_to :author, class_name: "User"
  
  has_one :joined_user_activity
  has_one :user, :through => :joined_user_activity

  has_many :tasks
  
  validates :name, presence: true
  validates :author, presence: true
end
