class Membership < ApplicationRecord
  validates :member, presence: :true
  validates :team, presence: :true
  validates_uniqueness_of :member, scope: :team, message: "user already part of this team"

  belongs_to :member, class_name: 'User'
  belongs_to :team
end
