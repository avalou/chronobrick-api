module Paginated
  DEFAULT_COUNT_BY_PAGE = 10

  def self.included(includer)
    includer.scope :paginate, -> (params) {
      count_by_page = includer.respond_to?(:count_by_page) ? includer.count_by_page : DEFAULT_COUNT_BY_PAGE
      page = (params.fetch :page, 0).to_i
      start = page * count_by_page
      offset(start).limit(count_by_page)
    }
  end
end
