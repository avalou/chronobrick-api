class Task < ApplicationRecord
  belongs_to :activity
  belongs_to :owner, class_name: "User"

  validates :name, presence: true
  validates :owner, presence: true
  validates :activity, presence: true
  validates :start_time, presence: true
end
