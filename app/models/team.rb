class Team < ApplicationRecord
    validates :name, uniqueness: true, presence: true

    has_many :memberships
    has_many :members, through: :memberships, dependent: :destroy
end
