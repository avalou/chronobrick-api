class User < ApplicationRecord
  validates :email, uniqueness: true, presence: true, :on => :create
  validates :username, uniqueness: true, presence: true, :on => :create
  validates :email, format:{ with: /\S+@\S+[.]\S+/, message: "Must be a valid email format." }
  validates :password_digest, presence: true

  has_many :created_activities, foreign_key: 'author_id', class_name: 'Activity', dependent: :destroy

  has_many :joined_user_activities
  has_many :activities, :through => :joined_user_activities

  has_many :tasks, foreign_key: 'owner_id', class_name: 'Task', dependent: :destroy
  
  has_many :memberships, foreign_key: :member_id, dependent: :destroy
  has_many :teams, through: :memberships
  
  has_secure_password
end
