class ActivitySerializer
  include JSONAPI::Serializer
  attributes :name, :author_id, :description, :client

  cache_options store: Rails.cache, namespace: 'jsonapi-serializer', expires_in: 1.hour
end
