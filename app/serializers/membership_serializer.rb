class MembershipSerializer
  include JSONAPI::Serializer
  attributes :team_id, :member_id, :can_edit
  
  cache_options store: Rails.cache, namespace: 'jsonapi-serializer', expires_in: 1.hour
end
