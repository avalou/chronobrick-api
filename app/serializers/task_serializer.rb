class TaskSerializer
  include JSONAPI::Serializer
  attributes :name, :description, :start_time, :end_time, :is_paid, :paid_by_id, :owner_id, :activity_id

  cache_options store: Rails.cache, namespace: 'jsonapi-serializer', expires_in: 1.hour
end
