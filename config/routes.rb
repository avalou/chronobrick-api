Rails.application.routes.draw do
  namespace :api, defaults: { format: :json }  do
    namespace :v1 do
      resources :users
      resources :tokens, only: %i[create]
      resources :teams do
        resources :memberships
      end
      resources :memberships, only: %i[index]
      resources :activities do
        resources :tasks
      end
      resources :tasks, only: %i[index]
    end
  end
end
