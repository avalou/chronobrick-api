class CreateActivities < ActiveRecord::Migration[6.1]
  def change
    create_table :activities do |t|
      t.string :name, index: true, null: false
      t.string :client
      t.string :description
      t.references :author, null: false

      t.timestamps
    end
  end
end
