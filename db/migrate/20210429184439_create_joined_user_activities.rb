class CreateJoinedUserActivities < ActiveRecord::Migration[6.1]
  def change
    create_table :joined_user_activities do |t|
      t.references :user
      t.references :activity

      t.timestamps
    end
  end
end
