class CreateTasks < ActiveRecord::Migration[6.1]
  def change
    create_table :tasks do |t|
      t.string :name, null: false
      t.index :name
      t.text :description
      t.datetime :start_time, null: false
      t.datetime :end_time, default: nil
      t.boolean :is_paid, default: false
      t.references :paid_by, default: nil
      t.references :activity, null: false
      t.references :owner, null: false

      t.timestamps
    end
  end
end
