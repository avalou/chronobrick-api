class CreateMemberships < ActiveRecord::Migration[6.1]
  def change
    create_table :memberships do |t|
      t.references :member
      t.references :team
      t.boolean :can_edit, default: false 

      t.timestamps
    end
  end
end
