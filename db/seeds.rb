User.delete_all
User.reset_pk_sequence
Activity.delete_all
Activity.reset_pk_sequence
JoinedUserActivity.delete_all
JoinedUserActivity.reset_pk_sequence
Task.delete_all
Task.reset_pk_sequence
Team.delete_all
Team.reset_pk_sequence
Membership.delete_all
Membership.reset_pk_sequence

3.times do |i|
  team = Team.create(name: Faker::Company.name, description: Faker::Company.catch_phrase)
  puts "Created TEAM ##{i+1} - #{team.name}"
end

10.times do |i|
  name = Faker::Name.first_name.downcase
  user = User.create(username: "#{name}", email: "#{name}@email.com", password: "azerty")
  puts "Created USER ##{i+1} - #{user.username}"
  
  2.times do
    activity = Activity.create(
      name: Faker::Science.element,
      client: Faker::Company.name,
      description: Faker::Company.catch_phrase,
      author_id: user.id
    )
    puts "Created ACTIVITY \"#{activity.name}\" for #{activity.client}"

    joined_user_activity = JoinedUserActivity.create(
      user_id: user.id,
      activity_id: activity.id
    )
    puts "Created ASSOCIATION ##{joined_user_activity.id}"
  end
  
  memberships = Membership.create(
    member_id: user.id,
    team_id: Team.all.sample.id,
    can_edit: true
  )
  # puts "USER ##{memberships.user_id} joined TEAM ##{memberships.team_id}"
end

User.create(username: "admin", email: "admin@email.com", password: "azerty", is_admin: true)
puts "Created Admin"

30.times do |i|
  task = Task.create(
    name: Faker::Verb.ing_form,
    description: Faker::Fantasy::Tolkien.poem,
    start_time: Faker::Time.between(from: DateTime.now - 1, to: DateTime.now),
    end_time: Faker::Time.between(from: DateTime.now + 1, to: DateTime.now + 5),
    owner_id: User.all.sample.id,
    activity_id: Activity.all.sample.id
  )
  puts "Created TASK ##{i+1} - #{task.name}"
end

