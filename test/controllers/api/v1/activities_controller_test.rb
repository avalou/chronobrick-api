require "test_helper"

class Api::V1::ActivitiesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @activity = activities(:one)
    @user = users(:one)
  end
  
  #INDEX
  test "should access index" do
    get api_v1_activities_url,
    headers: { Authorization: JsonWebToken.encode(user_id: @activity.author_id) },
    as: :json
    assert_response :success
  end

  test "should not access index" do
    get api_v1_activities_url, as: :json
    assert_response :forbidden
  end

  # test "should filter activities by name" do
  #   assert_equal 2, Activity.filter_by_name('pyheatpump').count
  # end

  # test "should filter activities by name and sort them" do
  #   assert_equal [activities(:one), activities(:three)], Activity.all.select {|n| n.name.include?("pyheat")}.sort {|a,b| a.name <=> b.name}
  # end

  #SHOW
  test "should access show" do
    get api_v1_activity_url(@activity),
    headers: { Authorization: JsonWebToken.encode(user_id: @activity.author_id) },
    as: :json
    assert_response :success

    json_response = JSON.parse(self.response.body)
    assert_equal @activity.name, json_response['data']['attributes']['name']
    assert_equal @activity.author_id, json_response['data']['attributes']['author_id']
    assert_equal @activity.description, json_response['data']['attributes']['description']
    assert_equal @activity.client, json_response['data']['attributes']['client']
  end

  test "should not access show" do
    get api_v1_activity_url(@activity),
    as: :json
    assert_response :forbidden
  end

  #CREATE
  test "should create activity" do
    assert_difference("Activity.count") do
      post api_v1_activities_url,
      params: { activity: { name: @activity.name, client: @activity.client, description: @activity.description, author_id: @user } },
      headers: { Authorization: JsonWebToken.encode(user_id: @activity.author_id) },
      as: :json
    end
    assert_response :created
  end

  test "should forbid create activity" do
    assert_no_difference "Activity.count" do
      post api_v1_activities_url,
      params: { activity: { name: @activity.name, client: @activity.client, description: @activity.description, author_id: @user } },
      as: :json
    end
    assert_response :forbidden
  end

  #UPDATE
  test "should update activity" do
    patch api_v1_activity_url(@activity),
    params: { activity: { name: "Updated name" } },
    headers: { Authorization: JsonWebToken.encode(user_id: @activity.author_id) },
    as: :json
    assert_response :success
  end

  test "should forbid update activity - not logged in" do
    patch api_v1_activity_url(@activity),
    params: { activity: { name: "Updated name" } },
    as: :json
    assert_response :forbidden
  end
  
  #DESTROY
  test "should destroy activity" do
    assert_difference "Activity.count", -1 do
    delete api_v1_activity_url(@activity),
    headers: { Authorization: JsonWebToken.encode(user_id: @activity.author_id) },
    as: :json
    end
    assert_response :no_content
  end

  test "should forbid destroy activity - not logged in" do
    assert_no_difference('Activity.count') do
    delete api_v1_activity_url(@activity),
    as: :json
    end
    assert_response :forbidden
  end
end