require "test_helper"

class Api::V1::MembershipsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @membership = memberships(:one)
    @user = users(:one)
    @user2 = users(:two)
    @team = teams(:one)
    @team2 = teams(:two)
  end

  # INDEX
  test "should access Membership index - specifix team" do
    get api_v1_team_memberships_url(@team),
    headers: { Authorization: JsonWebToken.encode(user_id: @user.id) }, 
    as: :json
    assert_response :success

    json_response = JSON.parse(self.response.body)
    assert_equal @membership.member_id, json_response['data'][0]['attributes']['member_id']
  end

  test "should access Membership index - all teams" do
    get api_v1_memberships_url,
    headers: { Authorization: JsonWebToken.encode(user_id: @user.id) }, 
    as: :json
    assert_response :success
  end

  test "should not access Membership index" do
    get api_v1_team_memberships_url(@team),
    as: :json
    assert_response :forbidden
  end

  # SHOW
  test "should show Membership" do
    get api_v1_team_memberships_url(@team, @membership),
    headers: { Authorization: JsonWebToken.encode(user_id: @user.id) }, 
    as: :json
    assert_response :success
  end

  test "should forbid show Membership" do
    get api_v1_team_memberships_url(@team, @membership),
    as: :json
    assert_response :forbidden
  end

  #CREATE
  test "should create membership" do
    assert_difference("Membership.count") do
      post api_v1_team_memberships_url(@team),
      params: { membership: { can_edit: false, member_id: @user2, team_id: @team } },
      headers: { Authorization: JsonWebToken.encode(user_id: @user2.id) }, 
      as: :json
    end
    assert_response :created
  end

  test "should forbid create membership" do
    assert_no_difference("Membership.count") do
      post api_v1_team_memberships_url(@team),
      params: { membership: { can_edit: false } },
      as: :json
    end
    assert_response :forbidden
  end

  # UPDATE
  test "should update membership" do
    patch api_v1_team_membership_url(@team, @membership),
    headers: { Authorization: JsonWebToken.encode(user_id: @user.id) }, 
    params: { membership: { can_edit: true } },
    as: :json
    assert_response :success
  end

  test "should forbid update membership" do
    patch api_v1_team_membership_url(@team, @membership),
    params: { membership: { can_edit: true } },
    as: :json
    assert_response :forbidden
  end

  test "should forbid update read_only attributes - team_id" do
    patch api_v1_team_membership_url(@team, @membership),
    headers: { Authorization: JsonWebToken.encode(user_id: @user.id) }, 
    params: { membership: { team_id: @team2.id, member_id: @user2.id } },
    as: :json
    assert_response :unprocessable_entity
  end

  # DESTROY
  test "should destroy membership" do
    assert_difference('Membership.count', -1) do
      delete api_v1_team_membership_url(@team, @membership), 
      headers: { Authorization: JsonWebToken.encode(user_id: @user.id) },
      as: :json
    end
    assert_response :no_content
  end

  test "should forbid destroy membership" do
    assert_no_difference('Membership.count') do
      delete api_v1_team_membership_url(@team, @membership), as: :json
    end
    assert_response :forbidden
  end
end
