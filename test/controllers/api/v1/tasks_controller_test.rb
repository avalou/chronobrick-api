require "test_helper"

class Api::V1::TasksControllerTest < ActionDispatch::IntegrationTest
  setup do
    @task = tasks(:one)
    @activity = activities(:one)
    @user = users(:one)
  end

  # INDEX
  test "should access index" do
    get api_v1_activity_tasks_url(@activity),
    headers: { Authorization: JsonWebToken.encode(user_id: @user.id) },
    as: :json
    assert_response :success
  end

  test "should not access index" do
    get api_v1_activity_tasks_url(@activity),
    as: :json
    assert_response :forbidden
  end

  # SHOW
  test "should access show" do
    get api_v1_activity_task_url(@activity, @task),
    headers: { Authorization: JsonWebToken.encode(user_id: @user.id) },
    as: :json
    assert_response :success

    json_response = JSON.parse(self.response.body)
    assert_equal @task.name, json_response['data']['attributes']['name']
    assert_equal @task.owner_id, json_response['data']['attributes']['owner_id']
    assert_equal @task.activity_id, json_response['data']['attributes']['activity_id']
  end

  test "should not access show" do
    get api_v1_activity_task_url(@activity, @task),
    as: :json
    assert_response :forbidden
  end

  # CREATE
  test "should create task" do
    assert_difference("Task.count") do
      post api_v1_activity_tasks_url(@activity),
      params: { task: { name: @task.name, description: @task.description, owner_id: @user, start_time: DateTime.now } },
      headers: { Authorization: JsonWebToken.encode(user_id: @user.id) },
      as: :json
    end
    assert_response :created
  end

  test "should not create task - not logged in" do
    assert_no_difference("Task.count") do
      post api_v1_activity_tasks_url(@activity),
      params: { task: { name: @task.name, description: @task.description, owner_id: @user } },
      as: :json
    end
    assert_response :forbidden
  end

  # UPDATE
  test "should update task" do
    patch api_v1_activity_task_url(@activity, @task),
    params: { task: { name: "New name", description: "New description" } },
    headers: { Authorization: JsonWebToken.encode(user_id: @user.id) },
    as: :json
    assert_response :success
  end

  test "should not update task" do
    patch api_v1_activity_task_url(@activity, @task),
    params: { task: { name: "New name", description: "New description" } },
    as: :json
    assert_response :forbidden
  end

  # DESTROY
  test "should destroy task" do
    assert_difference "Task.count", -1 do
    delete api_v1_activity_task_url(@activity, @task),
    headers: { Authorization: JsonWebToken.encode(user_id: @user.id) },
    as: :json
    end
    assert_response :no_content
  end

  test "should not destroy task" do
    assert_no_difference('Task.count') do
    delete api_v1_activity_task_url(@activity, @task),
    as: :json
    end
    assert_response :forbidden
  end
end
