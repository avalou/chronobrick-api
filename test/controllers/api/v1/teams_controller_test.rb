require "test_helper"

class Api::V1::TeamsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @team = teams(:one)
    @user = users(:one)
  end

  # INDEX
  test "should access team index" do
    get api_v1_teams_url,
    headers: { Authorization: JsonWebToken.encode(user_id: @user.id) }, 
    as: :json
    assert_response :success
  end

  test "should forbid team index" do
    get api_v1_teams_url, as: :json
    assert_response :forbidden
  end

  # SHOW
  test "should show team" do
    get api_v1_team_url(@team), 
    headers: { Authorization: JsonWebToken.encode(user_id: @user.id) }, 
    as: :json
    assert_response :success
  end

  test "should forbid show team" do
    get api_v1_team_url(@team),
    as: :json
    assert_response :forbidden
  end

  # CREATE
  test "should create team" do
    assert_difference('Team.count') do
      post api_v1_teams_url,
      params: { team: { name: "Random name", description: "Random description" } },
      headers: { Authorization: JsonWebToken.encode(user_id: @user.id) },
      as: :json
    end
    assert_response :created
  end

  test "should forbid create team" do
    assert_no_difference('Team.count') do
      post api_v1_teams_url,
      params: { team: { name: "Random name", description: "Random description" } },
      as: :json
    end
    assert_response :forbidden
  end

  test "should forbid create team with taken name" do
    assert_no_difference('Team.count') do
      post api_v1_teams_url,
      params: { team: { name: @team.name, description: "Random description" } },
      headers: { Authorization: JsonWebToken.encode(user_id: @user.id) },
      as: :json
    end
    assert_response :unprocessable_entity
  end

  test "should forbid create a team without name" do
    assert_no_difference('Team.count') do
      post api_v1_teams_url,
      params: { team: { description: "Random description"} },
      headers: { Authorization: JsonWebToken.encode(user_id: @user.id) },
      as: :json
    end
    assert_response :unprocessable_entity
  end
  
  # UPDATE
  test "should update team" do
    patch api_v1_team_url(@team),
    params: { team: { name: "New name", description: "New description" } },
    headers: { Authorization: JsonWebToken.encode(user_id: @user.id) },
    as: :json
    assert_response :success
  end

  test "should not update team " do
    patch api_v1_team_url(@team),
    params: { team: { name: "New name", description: "New description" } },
    as: :json
    assert_response :forbidden
  end

  # Ajouter un test pour vérifier le statut de "current_user.can_edit" dans le modèle Membership

  # DESTROY
  test "should destroy team" do
    assert_difference('Team.count', -1) do
      delete api_v1_team_url(@team),
      headers: { Authorization: JsonWebToken.encode(user_id: @user.id) },
      as: :json
    end
    assert_response :no_content
  end

  test "should forbid destroy team" do
    assert_no_difference('Team.count') do
      delete api_v1_team_url(@team), as: :json
    end
    assert_response :forbidden
  end
  # Ajouter un test pour vérifier le statut de "current_user.can_edit" dans le modèle Membership

end
