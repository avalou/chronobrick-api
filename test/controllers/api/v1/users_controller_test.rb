require "test_helper"

class Api::V1::UsersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @user = users(:one)
  end

  # INDEX
  test "should access user index" do
    get api_v1_users_url,
    headers: { Authorization: JsonWebToken.encode(user_id: @user.id) },
    as: :json
    assert_response :success 
  end

  test "should forbid user index" do
    get api_v1_users_url, as: :json
    assert_response :forbidden 
  end

  #SHOW 
  test "should show user" do
    get api_v1_user_url(@user),
    headers: { Authorization: JsonWebToken.encode(user_id: @user.id) },
    as: :json
    assert_response :success
    # Test to ensure response contains the correct email

    json_response = JSON.parse(self.response.body, symbolize_names: true)
    assert_equal @user.email, json_response.dig(:data, :attributes, :email)
    assert_equal @user.username, json_response.dig(:data, :attributes, :username)
  end

  test "should not show user" do
    get api_v1_user_url(@user),
    as: :json
    assert_response :forbidden
  end

  #CREATE
  test "should create user" do
    assert_difference('User.count') do
      post api_v1_users_url,
      params: { user: { email: 'test@test.org', username: 'new_user_name', password: '123456' } },
      as: :json
    end
    assert_response :created
  end

  test "should not create user with taken email" do
    assert_no_difference('User.count') do
      post api_v1_users_url,
      params: { user: { email: @user.email, username: 'username_test', password: '123456' } },
      as: :json
    end
    assert_response :unprocessable_entity
  end

  test "should not create user with taken username" do
    assert_no_difference('User.count') do
      post api_v1_users_url,
      params: { user: { email: "test@email.com", username: @user.username, password: '123456' } },
      as: :json
    end
    assert_response :unprocessable_entity
  end

  #UPDATE
  test "should update user" do
    patch api_v1_user_url(@user),
    params: { user: { email: @user.email, password: '123456' } },
    headers: { Authorization: JsonWebToken.encode(user_id: @user.id) },
    as: :json
    assert_response :success
  end

  test "should forbid update user" do
    patch api_v1_user_url(@user),
    params: { user: { email: @user.email, password: '123456' } },
    as: :json
    assert_response :forbidden
  end

  #DESTROY 
  test "should destroy user" do
    assert_difference('User.count', -1) do
      delete api_v1_user_url(@user),
      headers: { Authorization: JsonWebToken.encode(user_id: @user.id) },
      as: :json
    end
    assert_response :no_content
  end

  test "should forbid destroy user" do
    assert_no_difference('User.count') do
      delete api_v1_user_url(@user), as: :json
    end
    assert_response :forbidden
  end
end
