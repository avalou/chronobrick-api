require "test_helper"

class ActivityTest < ActiveSupport::TestCase
  test "activity's author should exist" do
    user = User.new(email: "test@test.com", username: "UserTest", password_digest: '$6$12$password')
    activity = Activity.new(name: "Pyheatpump", description: "A software to control heat pumps", author: user)
    assert activity.valid?
  end

  test "not valid if name does not exist" do
    user = User.new(email: "test@test.com", username: "UserTest", password_digest: '$6$12$password')
    activity = Activity.new(description: "A software to control heat pumps", author: user)
    assert_not activity.valid?
  end

  test "not valid if author does not exist" do
    activity = Activity.new(name: "Pyheatpump", description: "A software to control heat pumps", author: User.find_by_id(666))
    assert_not activity.valid?
  end
end