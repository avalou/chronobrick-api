require "test_helper"

class MembershipTest < ActiveSupport::TestCase
  setup do
    @membership = memberships(:one)
    @user = users(:one)
    @user2 = users(:two)
    @team = teams(:one)
    @team2 = teams(:two)
  end

  
  test "membership should be valid" do
    membership = Membership.new(member_id: @user2.id, team_id: @team2.id, can_edit: false)
    assert membership.valid?
  end

  test "membership without member_id should not be valid" do
    membership = Membership.new(team_id: @team.id, can_edit: true)
    assert_not membership.valid?
  end

  test "membership without team_id should not be valid" do
    membership = Membership.new(member_id: @user.id, can_edit: true)
    assert_not membership.valid?
  end

  test "already existing membership should not be created again" do
    membership = Membership.new(member_id: @user.id, team_id: @team.id, can_edit: true)
    assert_not membership.valid?
  end
end
