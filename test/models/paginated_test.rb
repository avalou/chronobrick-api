require "test_helper"
require_relative "../../app/models/paginated"

# fake model, fake behavior, etc.
class FakePaginated
  DATA = ("a".."z").to_a

  # fake inspection
  def self.data
    @@data
  end

  # fake a model
  def self.scope(scope_name, callback)
    define_singleton_method scope_name, callback
  end

  # fake a model
  def self.reset!
    @@data = DATA
  end

  # fake a model
  def self.offset(start)
    @@data = @@data[start..]
    self
  end

  # fake a model
  def self.limit(count)
    @@data = @@data[...count]
    self
  end

  def self.count_by_page
    3
  end

  include Paginated
end

class PaginatedTest < ActiveSupport::TestCase
  def assert_valid_pagination(paginated, count:)
    assert_equal count, paginated.data.size
  end

  test "paginated with placeholder" do
    FakePaginated.reset!
    test_empty_params = FakePaginated.paginate({})
    assert_valid_pagination test_empty_params, count: FakePaginated.count_by_page
    assert_equal %w[a b c], test_empty_params.data

    FakePaginated.reset!
    test_valid_params = FakePaginated.paginate({ page: 1 })
    assert_valid_pagination test_valid_params, count: FakePaginated.count_by_page
    assert_equal %w[d e f], test_valid_params.data
    FakePaginated.reset!
    test_valid_params = FakePaginated.paginate({ page: 4 })
    assert_valid_pagination test_valid_params, count: FakePaginated.count_by_page
    assert_equal %w[m n o], test_valid_params.data

    FakePaginated.reset!
    test_invalid_params = FakePaginated.paginate({ page: "what" })
    assert_valid_pagination test_invalid_params, count: FakePaginated.count_by_page
    assert_equal %w[a b c], test_invalid_params.data
  end
end
