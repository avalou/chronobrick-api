require "test_helper"

class TaskTest < ActiveSupport::TestCase
  setup do
    @user = users(:one)
    @activity = activities(:one)
  end

  test "task should be valid" do
    task = Task.new(name: "Write", description: "Write a new line", owner_id: @user.id, activity_id: @activity.id, start_time: DateTime.now)
    assert task.valid?
  end

  test "task should not be valid" do
    task = Task.new(name: "Write", description: "Write a new line")
    assert_not task.valid?
  end
end
