require "test_helper"

class TeamTest < ActiveSupport::TestCase
  test "team with valid name should be created" do
    team = Team.new(name: "Valid name", description: "Random description")
    assert team.valid?
  end

  test "team with taken name should not be created" do
    other_team = teams(:one)
    team = Team.new(name: other_team.name, description: "Random description")
    assert_not team.valid?
  end

  test "team without name should not be created" do
    team = Team.new(description: "Random description")
    assert_not team.valid?
  end
end
