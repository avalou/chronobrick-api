require "test_helper"

class UserTest < ActiveSupport::TestCase
  test 'user with a valid email should be valid' do
  user = User.new(email: "test@test.com", username: "UserTest", password_digest: 'password')
  assert user.valid?
  end

  test 'user with taken username should be invalid' do
  other_user = users(:one)
  user = User.new(email: "test@email.com", username: other_user.username, password_digest: "test_password")
  assert_not user.valid?
  end

  test 'user with invalid email should be invalid' do
  user = User.new(email: "test_invalid_email", username: "UserTest", password_digest: "test_password")
  assert_not user.valid?
  end

  test 'user with taken email should be invalid' do
  other_user = users(:one)
  user = User.new(email: other_user.email, username: "UserTest", password_digest: "test_password")
  assert_not user.valid?
  end
end
